package workr.redemption;

import io.javalin.Javalin;
import java.io.File;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;;

public class App {
    private static int getPortFromEnv() {
      String herokuPort = System.getenv("PORT");
      if (herokuPort != null) {
        return Integer.parseInt(herokuPort);
      }
      return 7070;
    }

    public static String handleMain(Configuration cfg) {
      try
      {
        // create a database connection
        Connection appConn = DriverManager.getConnection("jdbc:sqlite:sample.db");
        Statement statement = appConn.createStatement();
        statement.setQueryTimeout(30);  // set timeout to 30 sec.
        ResultSet rs = statement.executeQuery("select rowid, content from notes");
        HashMap<String, Object> root = new HashMap<>();
        HashMap<Integer, String> notes = new HashMap<Integer, String>();
        while(rs.next())
        {
          notes.put(rs.getInt("rowid"), rs.getString("content"));
        }
        root.put("notes", notes);

        Template temp = cfg.getTemplate("index.html");
        StringWriter out = new StringWriter();
        temp.process(root, out);
        return out.toString();
      }
      catch(Exception e)
      {
        // if the error message is "out of memory",
        // it probably means no database file is found
        System.err.println(e.getMessage());
      }
      return "Error";
    }
    public static void main(String[] args)
    {
      Configuration cfg = new Configuration(Configuration.VERSION_2_3_32);
      cfg.setAPIBuiltinEnabled(true);
        try {
          cfg.setDirectoryForTemplateLoading(new File("app/static"));
          cfg.setDefaultEncoding("UTF-8");
          cfg.setTemplateExceptionHandler(TemplateExceptionHandler.HTML_DEBUG_HANDLER);
          cfg.setWrapUncheckedExceptions(true);
        }
        catch (Exception e) {
          System.err.println(e.getMessage());
        }
        Connection connection = null;
        try
        {
          // create a database connection
          connection = DriverManager.getConnection("jdbc:sqlite:sample.db");
          Statement statement = connection.createStatement();
          statement.setQueryTimeout(30);  // set timeout to 30 sec.

          statement.executeUpdate("create table if not exists notes (content string)");
        }
        catch(SQLException e)
        {
          // if the error message is "out of memory",
          // it probably means no database file is found
          System.err.println(e.getMessage());
        }
        finally
        {
          try
          {
            if(connection != null)
              connection.close();
          }
          catch(SQLException e)
          {
            // connection close failed.
            System.err.println(e.getMessage());
          }
        }
        var app = Javalin.create(config -> {
            config.staticFiles.add(staticFiles -> {
                staticFiles.hostedPath = "/";
                staticFiles.directory = "static";
            });
        });
        app.get("/", ctx -> {
          ctx.html(handleMain(cfg));
        });

        app.post("/", ctx -> {
          try {
            Connection putConn = DriverManager.getConnection("jdbc:sqlite:sample.db");
            var formData = ctx.formParamMap();
            if (formData.containsKey("noteContent")) {
              // Create a new note
              String sql = "insert into notes values(?)";
              PreparedStatement p = putConn.prepareStatement(sql);
              p.setString(1, formData.get("noteContent").get(0));
              p.executeUpdate();
              ctx.html(handleMain(cfg));
          }
          else if (formData.containsKey("noteID")) {
              // Delete a note
              String sql = "DELETE FROM notes WHERE rowid = ?";
              var noteIDText = formData.get("noteID").get(0);
              var noteId = Integer.parseInt(noteIDText);
              PreparedStatement p = putConn.prepareStatement(sql);
              p.setInt(1, noteId);
              p.executeUpdate();
              ctx.html(handleMain(cfg));
          }
        }
        catch (Exception e) {
          System.err.println(e.getMessage());
          ctx.redirect("/");
        }
        });

        app.start(getPortFromEnv());
    }

}
